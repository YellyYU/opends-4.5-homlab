-2500			-2450	-2400	-2350	-2300	-2250	-2200

-1446.8824		-1300	-1250	-1200	-1150	-1100	-1050
-209.85168		-150	-100	-50		0		50		100
910.35254		950		1000	1050	1100	1150	1200
2061.9177		2100	2150	2200	2250	2300	2350
3213.216		3250	3300	3350	3400	3450	3500
4293.537		4350	4400	4450	4500	4550	4600


6062.1626		6100	6150	6200	6250	6300	6350
7072.9917		7100	7150	7200	7250	7300	7350
8227.518		8300	8350	8400	8450	8500	8550
9975.027		10000	10050	10100	10150	10200	10250
11201.857		11250	11300	11350	11400	11450	11500
12674.091		12700	12750	12800	12850	12900	12950

		<wayPoint id="WayPoint_M1C1"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>-2450</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M2C1"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>-1300</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M3C1"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>-150</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M4C1"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>950</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M5C1"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>2100</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M6C1"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>3250</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M7C1"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>4350</entry></vector></translation><speed>65</speed></wayPoint>

		<wayPoint id="WayPoint_M8C1"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>6100</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M9C1"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>7100</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M10C1"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>8300</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M11C1"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>10000</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M12C1"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>11250</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M13C1"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>12700</entry></vector></translation><speed>65</speed></wayPoint>

		<wayPoint id="WayPoint_C1END"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>4800</entry></vector></translation><speed>65</speed></wayPoint>

		
		
		
		<wayPoint id="WayPoint_M1C2"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>-2400</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M2C2"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>-1250</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M3C2"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>-100</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M4C2"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>1000</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M5C2"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>2150</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M6C2"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>3300</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M7C2"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>4400</entry></vector></translation><speed>65</speed></wayPoint>

		<wayPoint id="WayPoint_M8C2"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>6150</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M9C2"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>7150</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M10C2"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>8350</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M11C2"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>10050</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M12C2"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>11300</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M13C2"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>12750</entry></vector></translation><speed>65</speed></wayPoint>

		<wayPoint id="WayPoint_C2END"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>4800</entry></vector></translation><speed>65</speed></wayPoint>

		
		
		
		<wayPoint id="WayPoint_M1C3"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>-2350</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M2C3"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>-1200</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M3C3"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>-50</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M4C3"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>1050</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M5C3"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>2200</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M6C3"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>3350</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M7C3"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>4450</entry></vector></translation><speed>65</speed></wayPoint>

		<wayPoint id="WayPoint_M8C3"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.98</entry><entry>-0.44</entry><entry>6200</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M9C3"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.98</entry><entry>-0.44</entry><entry>7200</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M10C3"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.98</entry><entry>-0.44</entry><entry>8400</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M11C3"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.98</entry><entry>-0.44</entry><entry>10100</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M12C3"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.98</entry><entry>-0.44</entry><entry>11350</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M13C3"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.98</entry><entry>-0.44</entry><entry>12800</entry></vector></translation><speed>65</speed></wayPoint>

		<wayPoint id="WayPoint_C3END"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>4800</entry></vector></translation><speed>65</speed></wayPoint>



		<wayPoint id="WayPoint_M1C4"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>-2300</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M2C4"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>-1150</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M3C4"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>0</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M4C4"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>1100</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M5C4"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>2250</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M6C4"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>3400</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M7C4"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>4500</entry></vector></translation><speed>65</speed></wayPoint>

		<wayPoint id="WayPoint_M8C4"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>6250</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M9C4"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>7250</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M10C4"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>8450</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M11C4"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>10150</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M12C4"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>11400</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M13C4"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>12850</entry></vector></translation><speed>65</speed></wayPoint>

		<wayPoint id="WayPoint_4END"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>4800</entry></vector></translation><speed>65</speed></wayPoint>

		
		
		<wayPoint id="WayPoint_M1C5"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>-2250</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M2C5"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>-1100</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M3C5"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>50</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M4C5"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>1150</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M5C5"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>2300</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M6C5"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>3450</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M7C5"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>4550</entry></vector></translation><speed>65</speed></wayPoint>

		<wayPoint id="WayPoint_M8C5"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>6300</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M9C5"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>7300</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M10C5"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>8500</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M11C5"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>10200</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M12C5"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>11450</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M13C5"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>12900</entry></vector></translation><speed>65</speed></wayPoint>

		<wayPoint id="WayPoint_5END"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>4800</entry></vector></translation><speed>65</speed></wayPoint>


		
		<wayPoint id="WayPoint_M1C6"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>-2200</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M2C6"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>-1050</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M3C6"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>100</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M4C6"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>1200</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M5C6"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>2350</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M6C6"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>3500</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M7C6"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>4600</entry></vector></translation><speed>65</speed></wayPoint>

		<wayPoint id="WayPoint_M8C6"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>6350</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M9C6"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>7350</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M10C6"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>8550</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M11C6"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>10250</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M12C6"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>11500</entry></vector></translation><speed>65</speed></wayPoint>
		<wayPoint id="WayPoint_M13C6"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>12950</entry></vector></translation><speed>65</speed></wayPoint>

		<wayPoint id="WayPoint_6END"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>4800</entry></vector></translation><speed>65</speed></wayPoint>					





					
					
<wayPoint id="WayPoint_A07"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>-1750</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A08"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>-1650</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A09"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>-1350</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A10"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>-1100</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A11"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>-700</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A12"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>-450</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A13"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>-150</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A14"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>150</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A15"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>450</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A16"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>850</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A17"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>1100</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A18"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>1500</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A19"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>1900</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A20"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>2150</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A21"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>2450</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A22"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>2850</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A23"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>3100</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A24"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>3500</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A25"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>3800</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A26"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>4000</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A27"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>4300</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A28"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>4400</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A29"><translation><vector jtype="java_lang_Float" size="3"><entry>1.86</entry><entry>-0.44</entry><entry>4500</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A30"><translation><vector jtype="java_lang_Float" size="3"><entry>1.98</entry><entry>-0.44</entry><entry>4600</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A31"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>4700</entry></vector></translation><speed>65</speed></wayPoint>
<wayPoint id="WayPoint_A32"><translation><vector jtype="java_lang_Float" size="3"><entry>-1.88</entry><entry>-0.44</entry><entry>4800</entry></vector></translation><speed>65</speed></wayPoint>
