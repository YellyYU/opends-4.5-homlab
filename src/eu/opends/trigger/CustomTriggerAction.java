package eu.opends.trigger;

import eu.opends.basics.SimulationBasics;
import eu.opends.main.Simulator;

public class CustomTriggerAction extends TriggerAction
{
	boolean print;
	int duration;
	boolean toggleLane = false;

	private SimulationBasics sim;

	public CustomTriggerAction(SimulationBasics sim, boolean toggleLane)
	{
		super(0, 0);

		this.sim = sim;
		this.toggleLane = toggleLane;
	}

	/**
	 * Displays the specified message on the simulator screen for the given
	 * number of seconds.
	 */
	@Override
	protected void execute()
	{
		if (!isExceeded())
		{
			((Simulator) sim).getCar().setToggleEnable(toggleLane);
			updateCounter();
		}
	}

	@Override
	public String toString()
	{
		return "";
	}
}
