package eu.opends.trigger;

import eu.opends.basics.SimulationBasics;
import eu.opends.main.Simulator;
// STAR: added by umair

public class ToggleQNControleTriggerAction extends TriggerAction
{
	boolean print;
	int duration;
	boolean toggleControle = false;

	private SimulationBasics sim;

	public ToggleQNControleTriggerAction(SimulationBasics sim, boolean toggleControle)
	{
		super(0, 0);
		this.sim = sim;
		this.toggleControle = toggleControle;
	}

	/**
	 * Displays the specified message on the simulator screen for the given
	 * number of seconds.
	 */
	@Override
	protected void execute()
	{
		if (!isExceeded())
		{
			((Simulator) sim).toggleQNACTRControle();
			updateCounter();
		}
	}

	@Override
	public String toString()
	{
		return "";
	}
}