package eu.opends.trigger;

import eu.opends.basics.SimulationBasics;
import eu.opends.main.Simulator;

public class ToggleCruiseTriggerAction extends TriggerAction
{
	boolean print;
	int duration;
	boolean toggleCruise = false;

	private SimulationBasics sim;

	public ToggleCruiseTriggerAction(SimulationBasics sim, boolean toggleCruise)
	{
		super(0, 0);

		this.sim = sim;
		this.toggleCruise = toggleCruise;
	}

	/**
	 * Displays the specified message on the simulator screen for the given
	 * number of seconds.
	 */
	@Override
	protected void execute()
	{
		if (!isExceeded())
		{
			((Simulator) sim).getCar().setCruiseControl(toggleCruise);
			updateCounter();
		}
	}

	@Override
	public String toString()
	{
		return "";
	}
}
