package eu.opends.trigger;

import eu.opends.basics.SimulationBasics;
import eu.opends.main.Simulator;

public class PrintChunkTriggerAction extends TriggerAction {
	boolean print;
	int duration;
	boolean logEvent = false;

	private SimulationBasics sim;

	public PrintChunkTriggerAction(SimulationBasics sim, boolean logEvent) {
		super(0, 0);
		this.sim = sim;
		this.logEvent = logEvent;
	}

	/**
	 * Displays the specified message on the simulator screen for the given
	 * number of seconds.
	 */
	@Override
	protected void execute() {
		if (!isExceeded()) {

			if (logEvent)
				((Simulator) sim).toggleEventInProgress();
			else
				((Simulator) sim).setPrint(true);

			updateCounter();
		}
	}

	@Override
	public String toString() {
		return "";
	}
}
